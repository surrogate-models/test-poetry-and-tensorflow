import tensorflow as tf

def test_tf():
    print("TensorFlow version: ", tf.__version__)
    tf_model = tf.keras.Sequential(
        [
            tf.keras.layers.Dense(5, activation="tanh", input_shape=(2,)),
            tf.keras.layers.Dense(5, activation="tanh"),
            tf.keras.layers.Dense(1),
        ]
    )
    tf_model.compile(
        optimizer=tf.keras.optimizers.Adam(learning_rate=1e-3),
        loss=tf.keras.losses.MeanSquaredError(),
    )
