# Test Poetry and TensorFlow

[![pipeline status](https://gitlab.windenergy.dtu.dk/surrogate-models/test-poetry-and-tensorflow/badges/main/pipeline.svg)](https://gitlab.windenergy.dtu.dk/surrogate-models/test-poetry-and-tensorflow/-/commits/main)


This project is meant to test if Poetry can install TensorFlow on Windows.

The reason for this repo can be found in the following issues.

- https://github.com/tensorflow/io/issues/1789
- https://github.com/tensorflow/io/issues/1815
- https://github.com/tensorflow/io/issues/1832
- https://github.com/tensorflow/io/issues/1881
- https://github.com/tensorflow/io/issues/1906
- https://github.com/tensorflow/tensorflow/issues/58674
- https://github.com/tensorflow/tensorflow/issues/61477
- https://github.com/tensorflow/tensorflow/issues/62000
- https://github.com/tensorflow/tensorflow/issues/62899
